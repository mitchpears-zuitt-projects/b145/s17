console.log("hello from JS");

function productOutput(){
   let numA = parseInt(prompt('Enter a Product Number'));
   console.log(typeof numA);
   let numB = parseInt(prompt('Enter another Product Number')); 
   console.log(typeof numB);//this is to prove that we can directly pass the value of the variable and feed to the the parseInt method.
   
   //for this logic/structure .. it will only assess the whole number.

   //note: "else if" is 2 words

   let total = numA + numB
   if (total < 10) 
   		{console.log("The sum of the two numbers is " + (numA + numB));
alert('The sum of the two numbers is: ' +  (numA + numB)); 
   } else if (total < 20) 
   		{console.log("The difference of the two numbers is " + (numA - numB));
alert('The difference of the two numbers is: ' +  (numA - numB));
 
  } else if (total <= 21) 
   		{console.log("The product of the two numbers is " + (numA * numB));
alert('The product of the two numbers is: ' +  (numA * numB));
} else if (total >= 30) 
   		{console.log("The quotient of the two numbers is " + (numA / numB));
alert('The quotient of the two numbers is: ' +  (numA / numB));
}
}


function timeTraveler() {
     let name = prompt('Enter your Name');
   console.log(typeof name);
   let age = parseInt(prompt('Enter your age')); 
   console.log(typeof age);

if (name !== "" || age !== "") {
    console.log("Are you a time traveler?");
    alert('Are you a time traveler?');
} else if(name !== 'Mitch' && age < 100) {
    alert('Hello' + name + "." +  'Your age is'  + age + ".");
}
}
timeTraveler();

function getAgeStatus() {
     let unit = prompt('How old are you ?');
    let manggagamit;
    switch (unit) {
        case '18':
            manggagamit = "You are not allowed to party.";
            alert('You are not allowed to party.');
            break;
        case '21':
            manggagamit = "You are now part of the adult society.";
             alert('You are now part of the adult society.');
            break;
        case '65':
            console.log("We thank you for your contribution to society.");
            alert('We thank you for your contribution to society.');
            break;
        default:
            console.log("Are you sure you're not an alien?");
            alert('Are you sure you are not an alien?');
            break;
    }
    console.log(manggagamit); 
}
getAgeStatus();
 


	// body...
//create a function that will determine if the age is too old for preschool
function ageChecker(){
  //we are going to use a try-catch statement instead

  //get the input of the user.
  //the getElementById() will target a component within the document using its ID attribute
  //the "document" parameter describes the HTML document/file where the JS module is linked.
  // "value" => describes the value property of our elements
  let userInput = document.getElementById('age').value; 
  //alert(userInput); //checker
  //we will now target the element where we will display the output of this function.
  let message = document.getElementById('outputDisplay'); 
  console.log(typeof userInput); //string
  
  try {
  	//lets make sure that the input inserted by the user is NOT equals to a blank string.
  	//throw -> this statement examines the input and returns an error.
  	if (userInput === '' ) throw 'the input is empty';
  	//create a conditional statement that will check if the input in NOT a number 
  	//in order to check if the value is NOT A NUMBER, We will use a isNaN()
  	if (isNaN(userInput)) throw 'the input is Not a Number';
  	if (userInput <= 0) throw 'Not a valid Input' 
  	if (userInput <= 7) throw 'the input is good for preschool'; 
  	if (userInput > 7) throw 'too old for preschool';
  } catch(err) {
     //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
     //how are we going to inject a value inside the html container?
     //=> using innerHTML property : 
     //syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
     message.innerHTML = "Age Input: " + err; 
  } finally {
  	//this statement here will be executed regardless of the result above.
  	console.log('This is from the finally section');
  	//lalabas both ung block of code indicated in the finally section including the outcome of the try statement.
  }

}
